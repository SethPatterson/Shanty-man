<!-- SPDX-FileCopyrightText: 2024 Seth Patterson (NylaWoethief@disroot.org)

 SPDX-License-Identifier: Unlicense -->

# Shanty man page

A `man` page for Shanty, based on <https://nantucketebooks.com/shantydocs>.
